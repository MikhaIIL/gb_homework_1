package com.anufriev.mikhaiil.workoutapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

import static android.content.Context.MODE_PRIVATE;

public class WorkoutDetailFragment extends Fragment {
    private static final String DRAWABLE_PATH = "android.resource://com.anufriev.mikhaiil.workoutapp/drawable/";
    private static int itemNumber = 1;
    final private String ITEM_PREF = "item_";
    final private String RESULT_PREF = "_result";
    final private String DATE_PREF = "_date";
    ImageView exerciseImageView;
    TextView exerciseNameTextView;
    TextView exerciseDescriptionTextView;
    SeekBar repeatSeekBar;
    TextView repeatCountTextView;
    Button saveBestResultButton;
    Button shareBestResultButton;
    TextView bestRepeatResultTextView;
    TextView bestRepeatDateTextView;
    SharedPreferences sharedPref;
    private int bestRepeatResult = 0;
    private int currentItem = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.workout_exercise_detail, container, false);

        sharedPref = getActivity().getSharedPreferences("com.anufriev.mikhaiil.workoutapp", MODE_PRIVATE);
        initializeUI(root);

        repeatSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                String textVal = getResources().getString(R.string.repeat_count);
                repeatCountTextView.setText(textVal + String.valueOf(i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        saveBestResultButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentRepeats = Integer.parseInt(repeatCountTextView.getText().toString().replaceAll("\\D", ""));
                if (bestRepeatResultTextView.getText() != null && bestRepeatResult < currentRepeats) {
                    bestRepeatResult = currentRepeats;
                    String repeatText = getResources().getString(R.string.repeat_count);
                    String dateResultText = getResources().getString(R.string.just_date_word);
                    bestRepeatResultTextView.setText(repeatText + String.valueOf(bestRepeatResult));
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                    bestRepeatDateTextView.setText(dateResultText + dateFormat.format(new Date()));

                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putInt(ITEM_PREF + currentItem + RESULT_PREF, bestRepeatResult);
                    editor.putString(ITEM_PREF + currentItem + DATE_PREF, dateFormat.format(new Date()));
                    editor.apply();

                }
            }
        });

        shareBestResultButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String messageText = writeParamsInText(getResources().getStringArray(R.array.send_to_friend_best_result)
                        , String.valueOf(exerciseNameTextView.getText())
                        , String.valueOf(bestRepeatResult)
                );

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, messageText);
                String chooserTitle = getString(R.string.chooser_title);
                Intent chosenIntent = Intent.createChooser(intent, chooserTitle);
                startActivity(chosenIntent);
            }
        });

        return root;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void initializeUI(View root) {
        exerciseNameTextView = root.findViewById(R.id.exercise_name_text_view);
        exerciseImageView = root.findViewById(R.id.exercise_image_view);

        currentItem = itemNumber;
        String[] arr = getResources().getStringArray(R.array.list_of_exercises);
        exerciseNameTextView.setText(arr[currentItem]);

        repeatSeekBar = root.findViewById(R.id.repeat_seek_bar);
        repeatCountTextView = root.findViewById(R.id.repeat_count_text_view);

        exerciseDescriptionTextView = root.findViewById(R.id.exercise_description_text_view);
        String[] descriptionArr = getResources().getStringArray(R.array.list_of_exercises_description);
        exerciseDescriptionTextView.setText(descriptionArr[currentItem]);

        saveBestResultButton = root.findViewById(R.id.save_best_result_button);
        shareBestResultButton = root.findViewById(R.id.share_best_result_button);
        bestRepeatResultTextView = root.findViewById(R.id.best_result_repeats_text_view);
        bestRepeatDateTextView = root.findViewById(R.id.best_result_date_text_view);

        String[] imageFileName = getResources().getStringArray(R.array.list_of_exercises_image_names);
        exerciseImageView.setImageURI(Uri.parse(DRAWABLE_PATH + imageFileName[currentItem]));

        int savedResult = sharedPref.getInt(ITEM_PREF + currentItem + RESULT_PREF, 0);
        bestRepeatResult = savedResult;
        String repeatText = getResources().getString(R.string.repeat_count);
        bestRepeatResultTextView.setText(repeatText + String.valueOf(savedResult));
        String dateResultText = getResources().getString(R.string.just_date_word);
        String savedResultDate = sharedPref.getString(ITEM_PREF + currentItem + DATE_PREF, "");
        bestRepeatDateTextView.setText(dateResultText + savedResultDate);

    }

    public void setItemNumber(int itemIndex) {
        itemNumber = itemIndex;
    }

    private String writeParamsInText(String[] str, String... param) {
        String result = "";
        for (int i = 0; i < str.length; i++) {
            result += str[i] + ((param.length > i) ? param[i] : "");
        }
        return result;
    }
}
