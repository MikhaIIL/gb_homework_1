package com.anufriev.mikhaiil.workoutapp;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.List;

public class ExerciseList {

    private static final String DRAWABLE_PATH = "android.resource://com.anufriev.mikhaiil.workoutapp/drawable/";
    private static List<Exercise> exercises;
    private static ExerciseList exerciseList;
    final private String ITEM_PREF = "item_";
    final private String RESULT_PREF = "_result";
    final private String DATE_PREF = "_date";
    SharedPreferences sharedPreferences;
    private Context context;

    private ExerciseList(Context context) {
        this.context = context;
        exercises = initExerciseList();
    }

    public static ExerciseList getExerciseList(Context context) {
        exerciseList = null;
        if (exerciseList == null) {
            exerciseList = new ExerciseList(context);
        }
        return exerciseList;
    }

    public static List<Exercise> getExercises() {
        return exercises;
    }

    public static Exercise getExerciseByIndex(int index) {
        return exercises.get(index);
    }

    private List<Exercise> initExerciseList() {
        List<Exercise> exercises = new ArrayList<>();
        //sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences = context.getSharedPreferences("com.anufriev.mikhaiil.workoutapp", Context.MODE_PRIVATE);
        String[] exerciseNames = context.getResources().getStringArray(R.array.list_of_exercises);
        String[] exerciseDescription = context.getResources().getStringArray(R.array.list_of_exercises_description);
        String[] exerciseImages = context.getResources().getStringArray(R.array.list_of_exercises_image_names);

        for (int i = 0; i < exerciseNames.length; i++) {
            Exercise exercise = new Exercise();
            exercise.setName(exerciseNames[i]);
            exercise.setDescription(exerciseDescription[i]);
            exercise.setExerciseNum(i);

            int savedResult = sharedPreferences.getInt(ITEM_PREF + String.valueOf(i) + RESULT_PREF, 0);
            String repeatText = context.getResources().getString(R.string.repeat_count);
            exercise.setBestResultCount(repeatText + savedResult);

            String savedResultDate = sharedPreferences.getString(ITEM_PREF + String.valueOf(i) + DATE_PREF, "");
            String dateResultText = context.getResources().getString(R.string.just_date_word);
            exercise.setBestResultDate(dateResultText + savedResultDate);

            exercise.setImagePath(DRAWABLE_PATH + exerciseImages[i]);
            exercises.add(exercise);
        }
        return exercises;
    }
}
