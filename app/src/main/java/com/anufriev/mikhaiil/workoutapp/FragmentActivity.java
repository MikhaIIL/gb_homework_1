package com.anufriev.mikhaiil.workoutapp;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.anufriev.mikhaiil.workoutapp.interfaces.OnWorkoutListItemSelectListener;

import java.util.List;
import java.util.Locale;

public class FragmentActivity extends AppCompatActivity implements OnWorkoutListItemSelectListener {
    private static WorkoutListFragment listFragment;
    FragmentManager fragmentManager;
    Configuration config;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
        config = new Configuration(getResources().getConfiguration());
        listFragment = new WorkoutListFragment();
        fragmentManager = getSupportFragmentManager();
        if (config.orientation == Configuration.ORIENTATION_PORTRAIT) {
            fragmentManager.beginTransaction().replace(R.id.fragment_container, listFragment).commit();
        } else if (config.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            WorkoutDetailFragment detailFragment = new WorkoutDetailFragment();
            detailFragment.setItemNumber(0);
            fragmentManager.beginTransaction().replace(R.id.workout_list_container, listFragment).commit();
            fragmentManager.beginTransaction().replace(R.id.workout_detail_container, detailFragment).commit();
            //listFragment.setItemSelectedDefaultColor(0);
        }
    }

    @Override
    public void onWorkoutListItemSelected(int itemIndex) {
        WorkoutDetailFragment detailFragment = new WorkoutDetailFragment();
        detailFragment.setItemNumber(itemIndex);
        if (config.orientation == Configuration.ORIENTATION_PORTRAIT) {
            fragmentManager.beginTransaction().replace(R.id.fragment_container, detailFragment).addToBackStack(null).commit();
        } else if (config.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            fragmentManager.beginTransaction().replace(R.id.workout_detail_container, detailFragment).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String lang = Locale.getDefault().getLanguage();

        if (item.getItemId() == R.id.menu_ru_lang) {
            lang = "ru";
        } else if (item.getItemId() == R.id.menu_en_lang) {
            lang = "en";
        }
        config.locale = new Locale(lang);
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        List<Fragment> fragments = fragmentManager.getFragments();

        for (int i = 0; i < fragments.size(); i++) {
            transaction.detach(fragments.get(i));
            transaction.attach(fragments.get(i));
        }
        transaction.commit();
        return true;
    }

}
