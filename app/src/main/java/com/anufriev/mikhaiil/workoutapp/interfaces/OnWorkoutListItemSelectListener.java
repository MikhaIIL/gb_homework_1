package com.anufriev.mikhaiil.workoutapp.interfaces;

public interface OnWorkoutListItemSelectListener {
    void onWorkoutListItemSelected(int itemIndex);
}
