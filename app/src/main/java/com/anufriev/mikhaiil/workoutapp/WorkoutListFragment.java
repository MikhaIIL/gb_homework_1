package com.anufriev.mikhaiil.workoutapp;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anufriev.mikhaiil.workoutapp.interfaces.OnWorkoutListItemSelectListener;

import java.util.List;

public class WorkoutListFragment extends Fragment {
    public static final String WORKOUT_APP_INFO_LOG = "WorkoutAppLog";
    RecyclerView listOfExercisesRecyclerView;
    WorkoutExcerciseAdapter excerciseAdapter;
    FragmentTransaction transaction;
    FragmentManager fragmentManager;
    OnWorkoutListItemSelectListener workoutListListener;
    private int selectedItem = 0;
    private SparseArray<WorkoutExerciseViewHolder> exercisesListPositions = new SparseArray<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        selectedItem = 0;
        View root = inflater.inflate(R.layout.fragment_workout_list, container, false);
        ExerciseList.getExerciseList(getActivity());
        Log.d(WORKOUT_APP_INFO_LOG, "MainActivity was created");

        listOfExercisesRecyclerView = root.findViewById(R.id.workout_main_recycler_view);
        excerciseAdapter = new WorkoutExcerciseAdapter(getActivity(), ExerciseList.getExercises());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());

        listOfExercisesRecyclerView.setAdapter(excerciseAdapter);
        listOfExercisesRecyclerView.setLayoutManager(linearLayoutManager);

        return root;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser) {
            fragmentManager = getActivity().getSupportFragmentManager();
            transaction = fragmentManager.beginTransaction();
            transaction.detach(this);
            transaction.attach(this);
            transaction.commit();
        }
        super.setUserVisibleHint(isVisibleToUser);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        workoutListListener = (OnWorkoutListItemSelectListener) context;
    }

    @Override
    public void onStart() {
        Log.d(WORKOUT_APP_INFO_LOG, "MainActivity was started");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.d(WORKOUT_APP_INFO_LOG, "MainActivity on resume stage");
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.d(WORKOUT_APP_INFO_LOG, "MainActivity was paused");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.d(WORKOUT_APP_INFO_LOG, "MainActivity was stopped");
        super.onStop();
    }

    @Override
    public void onDestroy() {
        Log.d(WORKOUT_APP_INFO_LOG, "Destroying MainActivity");
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    static class WorkoutExerciseViewHolder extends RecyclerView.ViewHolder {
        CardView exerciseItemCardView;
        ImageView exerciseItemImageview;
        TextView exerciseItemTitleTextView;
        TextView exerciseItemDetailTextView;
        TextView bestExerciseResultTextView;
        TextView bestExerciseDateTextView;
        LinearLayout cardItem;
        int itemNumber;

        public WorkoutExerciseViewHolder(View exerciseItemView) {
            super(exerciseItemView);
            exerciseItemCardView = exerciseItemView.findViewById(R.id.exercise_list_item_card_view);
            exerciseItemImageview = exerciseItemView.findViewById(R.id.exercise_list_item_image_view);
            exerciseItemTitleTextView = exerciseItemView.findViewById(R.id.exercise_list_item_title_text_view);
            exerciseItemDetailTextView = exerciseItemView.findViewById(R.id.exercise_list_item_description_text_view);
            bestExerciseResultTextView = exerciseItemView.findViewById(R.id.exercise_list_item_record_repeats_count);
            bestExerciseDateTextView = exerciseItemView.findViewById(R.id.exercise_list_item_records_date);
            cardItem = exerciseItemView.findViewById(R.id.cardview_item);
        }

        public void setBackgroudColor(int color) {
            cardItem.setBackgroundColor(color);
        }
    }

    class WorkoutExcerciseAdapter extends RecyclerView.Adapter<WorkoutExerciseViewHolder> {
        Context context;
        List<Exercise> exercises;

        public WorkoutExcerciseAdapter(Context context, List<Exercise> exercises) {
            this.context = context;
            this.exercises = exercises;
        }

        @NonNull
        @Override
        public WorkoutExerciseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.workout_exercise_list,
                    parent, false);
            WorkoutExerciseViewHolder holder = new WorkoutExerciseViewHolder(itemView);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull final WorkoutExerciseViewHolder holder, int position) {
            Exercise exercise = exercises.get(holder.getAdapterPosition());
            holder.exerciseItemTitleTextView.setText(exercise.getName());
            holder.exerciseItemDetailTextView.setText(exercise.getDescription());
            holder.exerciseItemImageview.setImageURI(Uri.parse(exercise.getImagePath()));
            holder.bestExerciseResultTextView.setText(String.valueOf(exercise.getBestResultCount()));
            holder.bestExerciseDateTextView.setText(exercise.getBestResultDate());
            holder.itemNumber = exercise.getExerciseNum();

//            for (int i = 0; i < exercisesListPositions.size(); i++) {
//                if (exercisesListPositions.valueAt(i).itemNumber == holder.itemNumber) {
//                    selectedItem = holder.getAdapterPosition();
//                    exercisesListPositions.delete(exercisesListPositions.keyAt(i));
//                    exercisesListPositions.put(holder.getAdapterPosition(), holder);
//                } else {
//                    selectedItem = holder.getAdapterPosition();
//                    exercisesListPositions.put(holder.getAdapterPosition(), holder);
//                }
//
//            }
            holder.exerciseItemCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    workoutListListener.onWorkoutListItemSelected(holder.getAdapterPosition());

//                    for(int i = 0; i < exercisesListPositions.size(); i++) {
//                        exercisesListPositions.get(exercisesListPositions.keyAt(i)).setBackgroudColor(getResources().getColor(R.color.colorWhiteA95));
//                    }
//                    holder.setBackgroudColor(getResources().getColor(R.color.colorSandyBrownA90));
                }
            });
        }

        @Override
        public int getItemCount() {
            if (exercises != null && exercises.size() > 0) {
                return exercises.size();
            } else return 0;
        }
    }
}
