package com.anufriev.mikhaiil.workoutapp;

public class Exercise {
    private String name;
    private String description;
    private String bestResultCount;
    private String bestResultDate;
    private String imagePath;
    private int exerciseNum;

    public Exercise() {
    }

    public Exercise(String name, String description, String bestResultCount, String bestResultDate, String imagePath) {
        this.name = name;
        this.description = description;
        this.bestResultCount = bestResultCount;
        this.bestResultDate = bestResultDate;
        this.imagePath = imagePath;
    }

    public int getExerciseNum() {
        return exerciseNum;
    }

    public void setExerciseNum(int exerciseNum) {
        this.exerciseNum = exerciseNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBestResultCount() {
        return bestResultCount;
    }

    public void setBestResultCount(String bestResultCount) {
        this.bestResultCount = bestResultCount;
    }

    public String getBestResultDate() {
        return bestResultDate;
    }

    public void setBestResultDate(String bestResultDate) {
        this.bestResultDate = bestResultDate;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
